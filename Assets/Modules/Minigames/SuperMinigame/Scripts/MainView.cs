﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Minigame.UI
{
	public class MainView : MonoBehaviour
	{
		private const string CLICK_SFX_PATH = "Audio/Sfx/Pop";

		private AudioClip	clickSfx;
		private AudioSource	audioSrc;


		private void Awake ()
		{
			audioSrc = GetComponent <AudioSource> ();
			clickSfx = Resources.Load <AudioClip> (CLICK_SFX_PATH);
		}


		public void OnPlayClick ()
		{
			audioSrc.clip = clickSfx;
			audioSrc.Play ();
		}
	}
}

